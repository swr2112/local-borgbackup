#!/bin/bash

set -e

# The udev rule is not terribly accurate and may trigger our service before
# the kernel has finished probing partitions. Sleep for a bit to ensure
# the kernel is done.
#
# This can be avoided by using a more precise udev rule, e.g. matching
# a specific hardware path and partition.
sleep 5

# The backup partition is mounted there
MOUNTPOINT=/mnt/backups

# TODO this should just be automatically set if the filesystem is crypto_LUKS
IS_HDD_ENCRYPTED=true

# This is the location of the Borg repository
TARGET="$MOUNTPOINT/$(hostname).borg"

# Archive name schema
DATE=$(date --iso-8601)-$(hostname)

# This is the file that will later contain UUIDs of registered backup drives
DISKS=/etc/backups/backup.disks

# Find whether the connected block device is a backup drive
for uuid in $(lsblk --noheadings --list --output uuid)
do
        if grep --quiet --fixed-strings $uuid $DISKS; then
                break
        fi
        uuid=
done

if [ ! $uuid ]; then
        echo "No backup disk found, exiting"
        exit 0
fi

echo "Disk $uuid is a backup disk"
partition_path=/dev/disk/by-uuid/$uuid

if [ "$IS_HDD_ENCRYPTED" = true ]; then
	HDD_PASSPHRASE=$(cat /root/.hdd_passphrase)
        echo "Attempting to decrypt hard drive"
        echo $HDD_PASSPHRASE | cryptsetup luksOpen $partition_path crypt_backup - 
        partition_path=/dev/mapper/crypt_backup
        echo "Decrypted hard drive, using /dev/mapper/crypt_backup"
fi

# Mount file system if not already done. This assumes that if something is already
# mounted at $MOUNTPOINT, it is the backup drive. It won't find the drive if
# it was mounted somewhere else.
(mount | grep $MOUNTPOINT) || mount $partition_path $MOUNTPOINT
drive=$(lsblk --inverse --noheadings --list --paths --output name $partition_path | head --lines 1)
echo "Drive path: $drive"

BORG_OPTS="--stats --progress --one-file-system --compression lz4 --checkpoint-interval 86400"

export BORG_PASSCOMMAND="cat /root/.borg_passphrase"

# No one can answer if Borg asks these questions, it is better to just fail quickly
# instead of hanging.
export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no

# Log Borg version
borg --version

if [ -f /etc/backups/pre-backup.sh ]; then
  echo "Running pre-backup script"
  /etc/backups/pre-backup.sh
fi


echo "Starting backup for $DATE"

# system
borg create $BORG_OPTS \
  --exclude root/.cache \
  --exclude var/lib/docker/devicemapper \
  --exclude home \
  --exclude proc \
  --exclude dev \
  --exclude sys \
  --exclude tmp \
  $TARGET::$DATE-$$-system \
  / /boot

# /home 
borg create $BORG_OPTS \
  --exclude 'sh:home/*/.cache' \
  $TARGET::$DATE-$$-home \
  /home/

echo "Completed backup for $DATE"

# Just to be completely paranoid
sync

if [ -f /etc/backups/post-backup.sh ]; then
   echo "Running post-backup script"
   /etc/backups/post-backup.sh
fi


if [ "$IS_HDD_ENCRYPTED" = true ]; then
	cryptsetup luksClose /dev/mapper/crypt_backup
fi

if [ -f /etc/backups/autoeject ]; then
  umount $MOUNTPOINT
  hdparm -Y $drive
fi

if [ -f /etc/backups/backup-suspend ]; then
  systemctl suspend
fi

