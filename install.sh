#!/bin/bash

CONFIG_DIR=/etc/backups

if [ "$EUID" -ne 0 ]; then
  echo "This script must be run as root because it makes changes to /etc and creates a systemd service"
  exit 1;
fi

if [ -d $CONFIG_DIR ]; then
  echo "/etc/backups already exists; aborting. Remove dir or rename it"
  exit 1;
fi

chmod +x backups/run.sh

cp -r ./backups /etc/backups

ln -s /etc/backups/40-backup.rules /etc/udev/rules.d/40-backup.rules
ln -s /etc/backups/automatic-backup.service /etc/systemd/system/automatic-backup.service
systemctl daemon-reload
udevadm control --reload

echo /etc/backups and symlinks created


